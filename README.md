README
------
This is the Gitlab repository for the Dagstuhl Seminar entitled [Universals of Linguistic Idiosyncrasy in Multilingual Computational Linguistics](https://www.dagstuhl.de/21351) organised by:

  * [Timothy Baldwin](https://people.eng.unimelb.edu.au/tbaldwin/) (The University of Melbourne, Australia)
  * [William Croft](http://www.unm.edu/~wcroft/) (University of New Mexico – Albuquerque, USA)
  * [Joakim Nivre](https://stp.lingfil.uu.se/~nivre/) (Uppsala University, Sweden)
  * [Agata Savary](http://www.info.univ-tours.fr/~savary/) (University of Tours – France)

The seminar initially planned on <del>21-26 June 2020</del>, and rescheduled to <del>29 August to 3 September 2021</del> was finally reduced to a 2-day **online** workshop on **30-31 August 2021**. 

See the [wiki pages](https://gitlab.com/unlid/dagstuhl-seminar/wikis) for more details.

